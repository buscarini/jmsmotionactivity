# JMSMotionActivity

[![CI Status](http://img.shields.io/travis/José Manuel Sánchez/JMSMotionActivity.svg?style=flat)](https://travis-ci.org/José Manuel Sánchez/JMSMotionActivity)
[![Version](https://img.shields.io/cocoapods/v/JMSMotionActivity.svg?style=flat)](http://cocoadocs.org/docsets/JMSMotionActivity)
[![License](https://img.shields.io/cocoapods/l/JMSMotionActivity.svg?style=flat)](http://cocoadocs.org/docsets/JMSMotionActivity)
[![Platform](https://img.shields.io/cocoapods/p/JMSMotionActivity.svg?style=flat)](http://cocoadocs.org/docsets/JMSMotionActivity)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JMSMotionActivity is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "JMSMotionActivity"

## Author

José Manuel Sánchez, buscarini@gmail.com

## License

JMSMotionActivity is available under the MIT license. See the LICENSE file for more info.

