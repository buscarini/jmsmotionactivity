//
//  JMSViewController.h
//  JMSMotionActivity
//
//  Created by José Manuel Sánchez on 09/09/2014.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMSViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UILabel *numStepsLabel;

- (IBAction)start:(id)sender;

@end
