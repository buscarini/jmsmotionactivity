//
//  JMSAppDelegate.h
//  JMSMotionActivity
//
//  Created by CocoaPods on 09/09/2014.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
