//
//  main.m
//  JMSMotionActivity
//
//  Created by José Manuel Sánchez on 09/09/2014.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JMSAppDelegate class]));
    }
}
