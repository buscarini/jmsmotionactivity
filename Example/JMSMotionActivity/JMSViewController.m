//
//  JMSViewController.m
//  JMSMotionActivity
//
//  Created by José Manuel Sánchez on 09/09/2014.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "JMSViewController.h"

#import <JMSMotionActivity/JMSMotionActivity.h>

@interface JMSViewController ()

@property (nonatomic, strong) JMSMotionActivityManager *motionManager;

@end

@implementation JMSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	self.motionManager = [JMSMotionActivityManager new];
	__weak JMSViewController *wself = self;
	self.motionManager.motionTypeChangedBlock = ^(id sender, JMSMotionActivityType type) {
		wself.statusLabel.text = [JMSMotionActivityManager localizedStringForType:type];
	};
	
	self.motionManager.numStepsChangedBlock = ^(id sender,NSInteger numSteps) {
		wself.numStepsLabel.text = [NSString stringWithFormat:@"Steps: %ld",(long)numSteps];
	};
}

- (IBAction)start:(id)sender {
	self.startButton.selected = !self.startButton.selected;
	
	if (self.startButton.selected) {
		[self.motionManager start];
	}
	else {
		[self.motionManager stop];
	}
}

@end
