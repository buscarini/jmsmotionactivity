//
//  JMSMotionActivityManagerM7Impl.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/09/14.
//
//

#import "JMSMotionActivityManagerM7Impl.h"

@import CoreMotion;

@interface JMSMotionActivityManagerM7Impl()

@property (nonatomic,strong) CMMotionActivityManager *motionActivityManager;

@end

@implementation JMSMotionActivityManagerM7Impl

- (JMSMotionActivityType) motionTypeFromCMMotionType:(CMMotionActivity *) activity {
	if (activity.walking) return JMSMotionActivityWalking;
	else if (activity.running) return JMSMotionActivityRunning;
	else if (activity.automotive) return JMSMotionActivityAutomotive;
	else if (activity.stationary) return JMSMotionActivityNotMoving;
	else return JMSMotionActivityUnknown;
}

+ (BOOL) isAvailable {
	return [CMMotionActivityManager isActivityAvailable];
}


- (void) start {
	self.motionActivityManager = [[CMMotionActivityManager alloc] init];
	
	[self.motionActivityManager startActivityUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMMotionActivity *activity) {
		self.lastMotionType = [self motionTypeFromCMMotionType:activity];
		if (self.motionTypeChangedBlock) self.motionTypeChangedBlock(self,self.lastMotionType);
	}];
}

- (void) stop {
	[self.motionActivityManager stopActivityUpdates];
}

@end
