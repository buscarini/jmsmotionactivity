//
//  JMSMotionActivityManagerCoreMotionImpl.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/09/14.
//
//

#import "JMSMotionActivityManagerCoreMotionImpl.h"

#import "JMSMotionActivityHeader.h"

@import CoreMotion;

static const NSInteger maxLastValues = 3;
static const NSInteger maxLastValuesSumatory = 6; /// 3+2+1

static const NSTimeInterval maxWalkIntervalBetweenSteps = 2;
static const NSTimeInterval maxRunIntervalBetweenSteps = 0.5;

@interface JMSMotionActivityManagerCoreMotionImpl()

@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) NSOperationQueue *queue;

@property (nonatomic, strong) NSMutableArray *lastAccelerationPoints;
@property (nonatomic, assign) CMAcceleration lastAcceleration;

@property (nonatomic, strong) NSDate *lastStepDate;

@property (nonatomic, assign) NSInteger numSteps;

@end

@implementation JMSMotionActivityManagerCoreMotionImpl

@synthesize numSteps = _numSteps;

- (void) setNumSteps:(NSInteger)numSteps {
	_numSteps = numSteps;
	
	if (self.numStepsChangedBlock) self.numStepsChangedBlock(self,numSteps);
}

- (void) start {
	self.numSteps = 0;
	
	self.motionManager = [[CMMotionManager alloc] init];
	
	self.lastAccelerationPoints = [NSMutableArray array];
	
	self.motionManager.accelerometerUpdateInterval  = 1.0/10.0; // Update at 10Hz
	if (self.motionManager.accelerometerAvailable) {
		NSLog(@"Accelerometer available");
		
		self.queue = [NSOperationQueue currentQueue];
		[self.motionManager startAccelerometerUpdatesToQueue:self.queue
											withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
												
				// DDLogDebug(@"Accelerometer update");
												
				CMAcceleration acceleration = accelerometerData.acceleration;
           
				double d = [self calculateD:acceleration previousAcceleration:self.lastAcceleration];
				
				[self addDValue:d];
				
				double averageD = [self calculateAverageD:self.lastAccelerationPoints];
												
				JMSMotionActivityType previousMotionType = self.lastMotionType;
												
				self.lastMotionType = [self activityTypeFromDValue:averageD];
												
				self.lastAcceleration = acceleration;
												
				if (self.lastMotionType!=previousMotionType && self.motionTypeChangedBlock) self.motionTypeChangedBlock(self,self.lastMotionType);
		}];
	}
}

- (double) calculateD:(CMAcceleration) current previousAcceleration:(CMAcceleration) previous {
	
	double currentAux = sqrt(current.x*current.x+current.y*current.y+current.z*current.z);
	double previousAux = sqrt(previous.x*previous.x+previous.y*previous.y+previous.z*previous.z);
	
	double d = current.x*previous.x+current.y*previous.y+current.z*previous.z;
	d /= (currentAux*previousAux);
	
	return d;
}

- (void) addDValue:(double) d {
	
	if (self.lastAccelerationPoints.count>=maxLastValues) {
		[self.lastAccelerationPoints removeObjectAtIndex:0];
	}
	
	[self.lastAccelerationPoints addObject:@(d)];
}

- (double) calculateAverageD:(NSArray *) dValues {
	
	double result = 0;
	double multiplier = maxLastValues;
	for (NSNumber *dNumber in dValues) {
		result += (multiplier*[dNumber doubleValue]);
		multiplier--;
	}
	
	result /= maxLastValuesSumatory;
	
	return result;
}

- (JMSMotionActivityType) activityTypeFromDValue:(double) d {
	NSDate *previousStepDate = self.lastStepDate;
	
	if (d<0.98) {
		self.lastStepDate = [NSDate date];
		
		if (previousStepDate) {
			NSTimeInterval interval = [self.lastStepDate timeIntervalSinceDate:previousStepDate];

			if (interval<=self.motionManager.accelerometerUpdateInterval+0.01) {
				/// In this case we are probably still seeing the last step
				return self.lastMotionType;
			}
			
			self.numSteps++;
			
			if (interval<maxRunIntervalBetweenSteps) {
				return JMSMotionActivityRunning;
			}
			else if (interval<maxWalkIntervalBetweenSteps) {
				return JMSMotionActivityWalking;
			}
			else {
				return JMSMotionActivityNotMoving;
			}
		}
	}
	
	if (self.lastMotionType==JMSMotionActivityUnknown) self.lastMotionType = JMSMotionActivityNotMoving;
	return self.lastMotionType;
}

- (void) stop {
	[self.motionManager stopAccelerometerUpdates];
}

	
@end
