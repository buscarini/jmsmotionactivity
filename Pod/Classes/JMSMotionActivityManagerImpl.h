//
//  JMSMotionActivityManagerImpl.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/09/14.
//
//

#import <Foundation/Foundation.h>

#import "JMSMotionActivityTypes.h"

typedef void(^JMSMotionActivityTypeBlock)(id sender,JMSMotionActivityType type);

@interface JMSMotionActivityManagerImpl : NSObject

@property (nonatomic, copy) JMSMotionActivityTypeBlock motionTypeChangedBlock;

@property (nonatomic, assign) JMSMotionActivityType lastMotionType;

@property (nonatomic, copy) JMSMotionActivityNumStepsChangedBlock numStepsChangedBlock;
@property (nonatomic, readonly) NSInteger numSteps;

+ (BOOL) isAvailable;

- (void) start;
- (void) stop;

@end
