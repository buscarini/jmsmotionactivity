//
//  JMSMotionActivityManager.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/09/14.
//
//

#import "JMSMotionActivityManager.h"

#import "JMSMotionActivityManagerM7Impl.h"
#import "JMSMotionActivityManagerCoreMotionImpl.h"

@interface JMSMotionActivityManager()

@property (nonatomic, strong) JMSMotionActivityManagerImpl *impl;

@end

@implementation JMSMotionActivityManager

- (instancetype)init {
	self = [super init];
	if (self) {
		if ([JMSMotionActivityManagerM7Impl isAvailable]) {
			_impl = [JMSMotionActivityManagerM7Impl new];
		}
		else {
			_impl = [JMSMotionActivityManagerCoreMotionImpl new];
		}
	}
	return self;
}

+ (NSString *) localizedStringForType:(JMSMotionActivityType) type {
	if (type==JMSMotionActivityNotMoving) {
		return NSLocalizedString(@"Stationary", nil);
	}
	else if (type==JMSMotionActivityWalking) {
		return NSLocalizedString(@"Walking", nil);
	}
	else if (type==JMSMotionActivityRunning) {
		return NSLocalizedString(@"Running", nil);
	}
	else if (type==JMSMotionActivityAutomotive) {
		return NSLocalizedString(@"Automotive", nil);
	}
	
	return NSLocalizedString(@"Unknown", nil);
}

- (void) setMotionTypeChangedBlock:(JMSMotionActivityTypeBlock)motionTypeChangedBlock {
	self.impl.motionTypeChangedBlock = motionTypeChangedBlock;
}

- (JMSMotionActivityTypeBlock) motionTypeChangedBlock {
	return self.impl.motionTypeChangedBlock;
}

- (void) setNumStepsChangedBlock:(JMSMotionActivityNumStepsChangedBlock)numStepsChangedBlock {
	self.impl.numStepsChangedBlock = numStepsChangedBlock;
}

- (JMSMotionActivityNumStepsChangedBlock) numStepsChangedBlock {
	return self.impl.numStepsChangedBlock;
}

- (NSInteger) numSteps {
	return self.impl.numSteps;
}

- (void) start {
	[self.impl start];
}

- (void) stop {
	[self.impl stop];
}


@end
