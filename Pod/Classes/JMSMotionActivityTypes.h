//
//  JMSMotionActivityTypes.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/09/14.
//
//

#ifndef Pods_JMSMotionActivityTypes_h
#define Pods_JMSMotionActivityTypes_h

typedef NS_ENUM(NSUInteger, JMSMotionActivityType) {
	JMSMotionActivityNotMoving = 1,
	JMSMotionActivityWalking,
	JMSMotionActivityRunning,
	JMSMotionActivityAutomotive,
	JMSMotionActivityUnknown
};

typedef void(^JMSMotionActivityTypeBlock)(id sender,JMSMotionActivityType currentType);
typedef void(^JMSMotionActivityNumStepsChangedBlock)(id sender,NSInteger numSteps);

#endif
