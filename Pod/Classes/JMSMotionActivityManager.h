//
//  JMSMotionActivityManager.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/09/14.
//
//

#import <Foundation/Foundation.h>

#import "JMSMotionActivityTypes.h"

@class JMSMotionActivityManager;

@interface JMSMotionActivityManager : NSObject

@property (nonatomic, copy) JMSMotionActivityTypeBlock motionTypeChangedBlock;
@property (nonatomic, readonly) JMSMotionActivityType currentMotionType;

@property (nonatomic, copy) JMSMotionActivityNumStepsChangedBlock numStepsChangedBlock;
@property (nonatomic, readonly) NSInteger numSteps;

+ (NSString *) localizedStringForType:(JMSMotionActivityType) type;

- (void) start;
- (void) stop;

@end
