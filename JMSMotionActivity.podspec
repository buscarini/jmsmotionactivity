#
# Be sure to run `pod lib lint JMSMotionActivity.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "JMSMotionActivity"
  s.version          = "0.1.1"
  s.summary          = "A short description of JMSMotionActivity."
  s.description      = <<-DESC
                       A project for detecting using only the accelerometer if the user is walking, running or stationary.
                       DESC
  s.homepage         = "https://buscarini@bitbucket.org/buscarini/jmsmotionactivity.git"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "José Manuel Sánchez" => "buscarini@gmail.com" }
  s.source           = { :git => "https://buscarini@bitbucket.org/buscarini/jmsmotionactivity.git", :tag => "0.0.1" }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resource_bundles = {
    'JMSMotionActivity' => ['Pod/Assets/*.png']
  }

end
